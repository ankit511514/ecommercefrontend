import {HomepageComponent} from './homepage/homepage.component';
import {UserCartComponent} from './user-cart/user-cart.component';
import {Routes} from '@angular/router';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ProductListComponent} from './product-list/product-list.component';
import {AllproductsComponent} from './allproducts/allproducts.component';
import {AuthGuardServiceService} from './auth-guard-service.service';
import {LogoutComponent} from './logout/logout.component';
import {OrderhistoryComponent} from './orderhistory/orderhistory.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {AddProductComponent} from './add-product/add-product.component';
import {EditProductComponent} from './edit-product/edit-product.component';

// tslint:disable-next-line:variable-name
export const Main_Routes: Routes = [
  {
    path: 'home',
    component: HomepageComponent,
  },
  {
    path: 'cart',
    component: UserCartComponent, canActivate: [AuthGuardServiceService]
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'profile',
    component:  MyAccountComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'addproduct',
    component: AddProductComponent,
  },
  {
    path: 'editproduct/:id',
    component: EditProductComponent,
  },
  {
    path: 'logout',
    component: LogoutComponent, canActivate: [AuthGuardServiceService]
  },
  {
    path: 'productlist',
    component: ProductListComponent,
  },
  {
    path: 'allproduct',
    component: AllproductsComponent,
  },
  {
    path: 'productlist/:category',
    component: ProductListComponent,
  },
  {
    path: 'productdetails/:id',
    component: ProductDetailsComponent
  },
  {
    path: 'orderhistory',
    component: OrderhistoryComponent
  },
  {
    path: '**', redirectTo: 'login'
  },
  {
    path: '', redirectTo: 'login' , pathMatch: 'full'
  }
];
