import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ProductServiceService} from '../product-service.service';
import {CartserviceService} from '../cartservice.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,  private http: ProductServiceService , private cart: CartserviceService) {
  }
  // tslint:disable-next-line:ban-types
  private products: Object = [];
  private category;

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const categ = params.get('category');
      this.category = categ;
      this.http.getProductBycategory(this.category).subscribe(data => {
        this.products = data;
      }, (error => {console.log(error); }
      ));
    });
  }
  getProd(prodid) {
    this.router.navigate( ['productdetails/' , prodid]);
  }
  getProdByCatAndPrice(min, max) {
    this.http.getProductByCategoryAndPriceBetween( this.category, min , max).subscribe( data => {
      this.products = data;
    });
  }
  addThisToCart(id) {
    this.cart.addToCart(id).subscribe( (data) =>
      console.log(data));
    alert('Added to Cart');
  }
}

