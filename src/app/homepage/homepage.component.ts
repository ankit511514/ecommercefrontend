import { Component, OnInit , EventEmitter , Output } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor() { }
  private id;
  public products = [
    {
      id: 1,
      name: 'Audio-Technica ATH M50XBT',
      imagesrc: './assets/images/headphones2.jpg'
    },
    {
      id: 2,
      name: 'Sony wireless MX450 Headphones',
      imagesrc: './assets/images/headphones.JPG'
    },
    {
      id: 3,
      name: 'B&O play beoplay H4',
      imagesrc: './assets/images/headphones1.JPG'
    },
    {
      id: 4,
      name: 'Sony WH-XB700',
      imagesrc: './assets/images/headphones3.jpg'
    },
    {
      id: 5,
      name: 'Nike VaporMax PLus Sneakers',
      imagesrc: './assets/images/nike.jpg'
    },
    {
      id: 6,
      name: 'Nike VaporMax Flyknit Sneakers',
      imagesrc: './assets/images/nike1.JPG'
    },
    {
      id: 7,
      name: 'Nike Air Max',
      imagesrc: './assets/images/nike2.JPG'
    },
    {
      id: 8,
      name: 'Nike Jordans',
      imagesrc: './assets/images/nike3.JPG'
    },
  ];
  ngOnInit() {
  }

}
