import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductServiceService} from '../product-service.service';
import {UserServiceService} from '../user-service.service';
import {AuthenticationServiceService} from '../authentication-service.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  @Output() private childEvent = new EventEmitter();
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private productservice: ProductServiceService,
              private http: UserServiceService , private loginService: AuthenticationServiceService) { }

  private role;
  private user;
  private result;
  private searchedItem: string;
  ngOnInit() {
    this.http.getUser().subscribe( data => {
      this.user = data;
      this.role = this.user.role;
    });
  }
  searchOnClick() {
    console.log(this.searchedItem);
    // tslint:disable-next-line:triple-equals
    if (this.searchedItem != undefined && this.searchedItem != '') {
      this.productservice.getSearchedResult(this.searchedItem).subscribe(data => {
        /*const object = {
          result: undefined,
          search: undefined
        };
        object.search = this.searchedItem;*/
        this.result = data;
        // this.products = data;
        this.childEvent.emit(this.result);
      });
    }
  }
  getMyProfile(userid) {
    this.router.navigate( ['myprofile/' , userid]);
  }

}
