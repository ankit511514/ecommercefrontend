import { Component, OnInit } from '@angular/core';
import {Product, ProductServiceService} from '../product-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  private product = new Product();

  constructor(private productsService: ProductServiceService, private router: Router) { }

  ngOnInit() {
  }
  addProduct() {
    console.log(this.product);
    if (this.product.name != null && this.product.category != null && this.product.description != null && this.product.imgsr != null &&
      this.product.price != null) {
      if (this.product != null) {
        this.productsService.addProduct(this.product).subscribe(data => {
          alert('New Product Added Successfully');
          this.router.navigate(['/allproduct']);
        });
      }
    } else {
      alert('Please fill all the details.');
    }
  }

}
