import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

export class Product {
   id: number;
  name: string;
   imgsr: string;
   description: string;
   price: number;
  category: string;
}
@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {
 private url = 'http://localhost:1234/callproduct';
  constructor(private httpClient: HttpClient) { }
  getAllProduct(): Observable<Product> {
    return this.httpClient.get<Product>(this.url + '/products');
  }
  getProductBycategory(cat) {
    const headers = new HttpHeaders({ Authorization : 'Basic ' + btoa('anshul@gmail.com' + ':' + 'anshul') });
    return this.httpClient.get( this.url + '/showbycategory/' + cat , {headers});
  }
  getProductByid(Id) {
    const headers = new HttpHeaders({ Authorization : 'Basic ' + btoa('anshul@gmail.com' + ':' + 'anshul') });
    return this.httpClient.get( this.url + '/showbyid/' + Id , {headers});
  }
  getProductByCategoryAndPriceBetween(categ, min , max) {
    const headers = new HttpHeaders({ Authorization : 'Basic ' + btoa('anshul@gmail.com' + ':' + 'anshul') });
    return this.httpClient.get( this.url + '/products-cat/' + categ + '/' + min + '/' + max, {headers});
  }
  getProductByPriceBetween(min , max) {
    const headers = new HttpHeaders({ Authorization : 'Basic ' + btoa('anshul@gmail.com' + ':' + 'anshul') });
    return this.httpClient.get( this.url + '/all-products/' + min + '/' + max, {headers});
  }
  getAllProducts() {
    const headers = new HttpHeaders({ Authorization : 'Basic ' + btoa('anshul@gmail.com' + ':' + 'anshul') });
    return this.httpClient.get( this.url + '/show' , {headers});
  }
  addProduct(product) {
    const headers = new HttpHeaders({Authorization: sessionStorage.getItem('basicAuth')});
    return this.httpClient.post(this.url + '/put', product, {headers});
  }

  removeFromDB(id) {
    const headers = new HttpHeaders( {Authorization: sessionStorage.getItem('basicAuth')});
    return this.httpClient.get(this.url + '/delete/' + id , {headers});
  }

  editProductDetails(product) {
    const headers = new HttpHeaders({Authorization: sessionStorage.getItem('basicAuth')});
    return this.httpClient.post<Product>(this.url + '/editProduct' , product, {headers});
  }

  getSearchedResult(searchedItem) {
    const headers = new HttpHeaders({Authorization: sessionStorage.getItem('basicAuth')});
    return this.httpClient.get(this.url + '/search/' + searchedItem, {headers});
  }
}
