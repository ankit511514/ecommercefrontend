import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ProductServiceService} from '../product-service.service';
import {AuthenticationServiceService} from '../authentication-service.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  private productId;
  private product;
  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductServiceService,
              private loginService: AuthenticationServiceService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      // tslint:disable-next-line:radix
      const id = parseInt(params.get('id'));
      this.productId = id;
    });

    this.productService.getProductByid(this.productId).subscribe(data => this.product = data);
  }

  editProduct() {
    console.log(this.product);
    this.productService.editProductDetails(this.product).subscribe(data => {
      this.product = data;
      alert('Product Details updated successfully.');
      this.router.navigate(['products/all']);
    });
  }
}
