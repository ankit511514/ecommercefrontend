import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ProductServiceService} from '../product-service.service';
import {CartserviceService} from '../cartservice.service';
import {AuthenticationServiceService} from '../authentication-service.service';
import {UserServiceService} from '../user-service.service';

@Component({
  selector: 'app-allproducts',
  templateUrl: './allproducts.component.html',
  styleUrls: ['./allproducts.component.scss']
})
export class AllproductsComponent implements OnInit {
  private role;
  private user;
  constructor(private route: ActivatedRoute, private router: Router, private http: ProductServiceService,
              private loginService: AuthenticationServiceService, private cart: CartserviceService
  ,           private userservice: UserServiceService) { }

  private products: object = [];
  private category;

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.http.getAllProducts().subscribe(data => {
        this.products = data;
      }, (error => {console.log(error); }
      ));
    });
    this.userservice.getUser().subscribe( data => {
      this.user = data;
      this.role = this.user.role;
    });
  }
  getProd(prodid) {
    this.router.navigate( ['productdetails/' , prodid]);
  }
  getProdByPrice(min, max) {
    this.http.getProductByPriceBetween( min , max).subscribe( data => {
      this.products = data;
    });
  }
  addThisToCart(id) {
    this.cart.addToCart(id).subscribe( (data) =>
      console.log(data));
    alert('Added to Cart');
  }
  removeProduct(product) {
    alert('Product Deleted Successfully');
    this.http.removeFromDB(product).subscribe((data) => {
      this.products = data;

    });
  }

  editProduct(product) {
    this.router.navigate(['editproduct/', product]);
  }
}
