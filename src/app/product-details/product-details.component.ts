import { Component, OnInit , Input } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ProductServiceService} from '../product-service.service';
import {CartserviceService} from '../cartservice.service';
import {AuthenticationServiceService} from '../authentication-service.service';
import {UserServiceService} from '../user-service.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  @Input() public productid;
  constructor(private route: ActivatedRoute, private router: Router, private http: ProductServiceService,
              private cart: CartserviceService , private loginService: AuthenticationServiceService ,) { }
  private products;
  private id;
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const idd = params.get('id');
      this.id = idd;
      this.http.getProductByid(this.id).subscribe(data => {
        this.products = data;
      }, (error => {console.log(error); }
      ));
    });
  }
  addThisToCart(id) {
    this.cart.addToCart(id).subscribe( (data) =>
    console.log(data));
    alert('Added to Cart');
  }

}
