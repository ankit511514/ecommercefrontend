import { Component, OnInit } from '@angular/core';
import {User, UserServiceService} from '../user-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
  user: User;
  private name;
  private role;
  private phonenumber;
  private email;
  private username;

  constructor(private registrationService: UserServiceService, private router: Router) { }

  ngOnInit() {
    this.registrationService.getUser().subscribe( data => {
      console.log(data);
      this.user = data;
      this.email = this.user.email;
      this.name = this.user.name;
      this.phonenumber = this.user.phonenumber;
      this.role = this.user.role;
      this.username = this.username;
      console.log(this.user);
    });
  }
  editUser() {
    this.registrationService.editMyUser(this.user).subscribe(data => {
      this.user = data;
      alert('Details updated successfully.');
      this.router.navigate(['home']);
    });
  }

}
